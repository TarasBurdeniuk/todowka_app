import * as React from 'react';
import { Platform, UIManager, useColorScheme } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import LoginScreen from '../screens/LoginScreen/LoginScreen';
import RegisterScreen from '../screens/RegisterScreen/RegisterScreen';
import ProfileScreen from '../screens/ProfileScreen/ProfileScreen';
import SettingsScreen from '../screens/SettingsScreen/SettingsScreen';
import { DarkTheme, LightTheme } from '../constants/themes';
import linking from './linking';

const Stack = createNativeStackNavigator();

if (Platform.OS === 'android') {
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
}

const Routes = () => {
  const scheme = useColorScheme();
  return (
    <NavigationContainer theme={scheme === 'dark' ? DarkTheme : LightTheme} linking={linking}>
      <Stack.Navigator initialRouteName='LoginScreen'>
        <Stack.Screen
          name='LoginScreen'
          component={LoginScreen}
          options={{
            header: () => null,
          }}
        />
        <Stack.Screen
          name='RegisterScreen'
          component={RegisterScreen}
          options={{
            header: () => null,
          }}
        />
        <Stack.Screen name='ProfileScreen' component={ProfileScreen} />
        <Stack.Screen name='SettingsScreen' component={SettingsScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routes;
