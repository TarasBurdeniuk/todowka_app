const colors = {
  // dark theme colors
  mainDarkBackground: '#120B25',
  textDark: '#dadbe0',

  // light theme colors
  mainLightBackground: '#ffffff',
  textLight: '#12043d',
};

const DarkTheme = {
  // dark: true,
  colors: {
    primary: 'rgb(255, 45, 85)',
    background: colors.mainDarkBackground,
    card: 'rgb(52,46,70)',
    text: colors.textDark,
    border: 'rgb(199, 199, 204)',
    notification: 'rgb(255, 69, 58)',
  },
};

const LightTheme = {
  // dark: false,
  colors: {
    primary: 'rgb(255, 45, 85)',
    background: colors.mainLightBackground,
    card: 'rgb(255, 255, 255)',
    text: colors.textLight,
    border: 'rgb(21,21,21)',
    notification: 'rgb(255, 69, 58)',
  },
};

export { DarkTheme, LightTheme, colors };
