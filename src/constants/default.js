const styles = {
  fontSize: 12,
  fontFamilyBold: 'Kotyhoroshko-Bold',
  fontFamilyRegular: 'Kotyhoroshko-Regular',
};

const url = {
  dev: '',
  prod: '',
};

export { styles, url };
