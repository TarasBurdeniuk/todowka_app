const config = {
  screens: {
    ProfileScreen: {
      path: 'profile/:id',
      parse: {
        id: id => `${id}`,
      },
    },
    SettingScreen: 'setting',
    RegisterScreen: 'register',
    LoginScreen: 'login',
  },
};

const linking = {
  prefixes: ['todowka://app', 'https://www.todowka.com'],
  config,
};

export default linking;
