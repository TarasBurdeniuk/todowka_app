import React from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import TextBold from '../../components/ui/TextBold';

const ProfileScreen = ({ route }) => {
  const { id } = route.params;
  return (
    <SafeAreaView>
      <TextBold style={styles.text}>Profile Screen</TextBold>
      <TextBold style={styles.text}>Deep linking id: {id}</TextBold>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 16,
  },
});

export default ProfileScreen;
