import React from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import TextBold from '../../components/ui/TextBold';

const SettingsScreen = () => (
    <SafeAreaView>
      <TextBold style={styles.text}>Settings Screen</TextBold>
    </SafeAreaView>
  );

const styles = StyleSheet.create({
  text: {
    fontSize: 16,
  },
});

export default SettingsScreen;
